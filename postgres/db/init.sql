--
--Archivo para la creacion de las bases de datos de los
--diferentes servicios
--

CREATE DATABASE usersdb
 WITH
  OWNER = "Base_c#le"
  ENCODING = 'UTF8'
  LC_COLLATE = 'en_US.utf8'
  LC_CTYPE = 'en_US.utf8'
  TABLESPACE = pg_default
  CONNECTION LIMIT = -1;


CREATE DATABASE cursosdb
 WITH
  OWNER = "Base_c#le"
  ENCODING = 'UTF8'
  LC_COLLATE = 'en_US.utf8'
  LC_CTYPE = 'en_US.utf8'
  TABLESPACE = pg_default
  CONNECTION LIMIT = -1;


CREATE DATABASE filesdb
WITH
 OWNER = "Base_c#le"
 ENCODING = 'UTF8'
 LC_COLLATE = 'en_US.utf8'
 LC_CTYPE = 'en_US.utf8'
 TABLESPACE = pg_default
 CONNECTION LIMIT = -1;


CREATE DATABASE homeworkdb
WITH
 OWNER = "Base_c#le"
 ENCODING = 'UTF8'
 LC_COLLATE = 'en_US.utf8'
 LC_CTYPE = 'en_US.utf8'
 TABLESPACE = pg_default
 CONNECTION LIMIT = -1;
